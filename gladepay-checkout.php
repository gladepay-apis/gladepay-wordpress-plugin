<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://gladepay.com
 * @since             1.0.0
 * @package           GladePayCheckout
 *
 * @wordpress-plugin
 * Plugin Name:       Gladepay Checkout
 * Plugin URI:        https://developer.gladepay.com/sdk-plugins.php
 * Description:       Easily add Gladepay's payment form to any page or post using simple shortcode. A few other customization options are available as well.
 * Version:           1.0.0
 * Author:            Gladepay
 * Author URI:        http://gladepay.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       Gladepay Checkout
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GLADEPAY_CHECKOUT_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gladepay-checkout-activator.php
 */
function activate_gladepay_checkout() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gladepay-checkout-activator.php';
	GladePayCheckout_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gladepay-checkout-deactivator.php
 */
function deactivate_gladepay_checkout() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gladepay-checkout-deactivator.php';
	GladePayCheckout_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gladepay_checkout' );
register_deactivation_hook( __FILE__, 'deactivate_gladepay_checkout' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gladepay-checkout.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gladepay_checkout() {

	$plugin = new GladePayCheckout();
	$plugin->run();

}
run_gladepay_checkout();



