<?php

/**
 * Provides the markup for a select field
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 *
 * @package    GladepayCheckout
 * @subpackage GladepayCheckout/admin/partials
 * @author     Gladepay <support@gladepay.com>
 */

if ( ! empty( $atts['label'] ) ) {

	?><label for="<?php echo esc_attr( $atts['id'] ); ?>"><?php esc_html_e( $atts['label'], 'employees' ); ?>: </label><?php

}

?><select
	aria-label="<?php esc_attr( _e( $atts['aria'], 'gladepay-checkout' ) ); ?>"
	class="<?php echo esc_attr( $atts['class'] ); ?>"
	id="<?php echo esc_attr( $atts['id'] ); ?>"
	name="<?php echo esc_attr( $atts['name'] ); ?>"><?php

if ( ! empty( $atts['blank'] ) ) {

	?><option value><?php esc_html_e( $atts['blank'], 'gladepay-checkout' ); ?></option><?php

}

foreach ( $atts['selections'] as $selection ) {

	if ( is_array( $selection ) ) {

		$label = $selection['label'];
		$value = $selection['value'];

	} else {

		$label = strtolower( $selection );
		$value = strtolower( $selection );

	}

	?><option
		value="<?php echo esc_attr( $value ); ?>" <?php
		selected( $atts['value'], $value ); ?>><?php

		esc_html_e( $label, 'gladepay-checkout' );

	?></option><?php

} // foreach

?></select><br>
<span class="description"><?php esc_html_e( $atts['description'], 'gladepay-checkout' ); ?></span>
</label>
