<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 *
 * @package    GladepayCheckout
 * @subpackage GladepayCheckout/admin/partials
 * @author     Gladepay <support@gladepay.com>
 */

?><h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
<h2><?php esc_html_e( 'Shortcode', 'gladepay' ); ?></h2>
<p><?php esc_html_e( 'The simplest version of the shortcode is:', 'gladepay' ); ?></p>
<pre><code>[gladepay]</code></pre>
<p><?php esc_html_e( 'Enter that in the Editor on any page or post to display the payment form.', 'gladepay' ); ?></p>
<p><?php esc_html_e( 'This is an example with all the default attributes used:', 'gladepay' ); ?></p>
<pre><code>[gladepay firstname="John" lastname="Doe" email="doe@example.com" phone="08098765432" amount="20000" currency="NGN" country="NG"]</code></pre>

<h3><?php esc_html_e( 'Shortcode Attributes', 'gladepay' ); ?></h3>
<p><?php esc_html_e( 'There are currently seven attributes that can be added to the shortcode to help receiving your payments:', 'gladepay' ); ?></p>
<ol>
	<li><?php esc_html_e( 'firstname', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'lastname', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'email', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'phone', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'amount', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'currency', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'country', 'gladepay' ); ?></li>
</ol>

<h4><?php esc_html_e( 'firstname', 'gladepay' ); ?></h4>
<p><?php  esc_html_e( "This will be the first name of your customer, but you can skip this attribute if you don't want a fixed last name", 'gladepay' ); ?></p>

<h4><?php esc_html_e( 'lastname', 'gladepay' ); ?></h4>
<p><?php  esc_html_e( "This will be the last name of your customer, but you can skip this attribute if you don't want a fixed last name", 'gladepay' ); ?></p>

<h4><?php esc_html_e( 'email', 'gladepay' ); ?></h4>
<p><?php  esc_html_e( "This will be the email address of your customer, we will send a payment receipt to this email. So, it is required of user to fill this part on the form.", 'gladepay' ); ?></p>

<h4><?php esc_html_e( 'phone', 'gladepay' ); ?></h4>
<p><?php  esc_html_e( "This will be the phone of your customer, but you can skip this attribute if you don't want a phone number", 'gladepay' ); ?></p>

<h4><?php esc_html_e( 'amount', 'gladepay' ); ?></h4>
<p><?php  esc_html_e( "This will be the amount you want your customer to pay, but you can skip this attribute if you don't want your customer to pay a fixed amount.", 'gladepay' ); ?></p>
<p><?php esc_html_e( 'Examples of the amount attribute:', 'gladepay' ); ?></p>
<ul>
	<li><?php esc_html_e( 'amount="2000" (will prompt your customer pay an amount value of of 2000 on the form).', 'gladepay' ); ?></li>
</ul>

<h4><?php esc_html_e( 'currency', 'gladepay' ); ?></h4>
<p><?php  esc_html_e( "Determines the currency you want your customers to pay with.", 'gladepay' ); ?></p>
<p><?php esc_html_e( 'Examples of the currency attribute:', 'gladepay' ); ?></p>
<ul>
	<li><?php esc_html_e( 'currency="NGN" (customers pay in Nigeran Naira)', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'currency="USD" (customers pay in US Dollars)', 'gladepay' ); ?></li>
	<!-- <li><?php esc_html_e( 'currency="KES" (customers pay in Kenyan Shilling)', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'currency="GHS" (customers pay in Ghana Cedi)', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'currency="CFA" (customers pay in West African CFA franc)', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'currency="GBP" (customers pay in British Pound Sterling)', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'currency="EUR" (customers pay in EURO)', 'gladepay' ); ?></li> -->
</ul>

<h4><?php esc_html_e( 'country', 'gladepay' ); ?></h4>
<p><?php esc_html_e( "Determines which country you want to receive payments with", 'gladepay' ); ?></p>
<p><?php esc_html_e( 'Examples of the country attribute:', 'gladepay' ); ?></p>
<ul>
	<li><?php esc_html_e( 'country="NG" (Nigeria)', 'gladepay' ); ?></li>
	<!-- <li><?php esc_html_e( 'country="KYN" (Kenya)', 'gladepay' ); ?></li>
	<li><?php esc_html_e( 'country="SA" (South Africa)', 'gladepay' ); ?></li> -->
</ul>


<h4><?php esc_html_e( 'Contact Us', 'gladepay' ); ?></h4>
<p><?php printf( wp_kses( __( 'For further questions or enquiries, please visit our <a href="%1$s">website</a>.', 'gladepay' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( 'https://gladepay.com' ) ); ?></p>