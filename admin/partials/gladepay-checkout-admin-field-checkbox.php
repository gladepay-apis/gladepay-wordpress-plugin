<?php

/**
 * Provides the markup for any checkbox field
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 *
 * @package    GladepayCheckout
 * @subpackage GladepayCheckout/admin/partials
 * @author     Gladepay <support@gladepay.com>
 */

?><label for="<?php echo esc_attr( $atts['id'] ); ?>">
	<input aria-role="checkbox"
		<?php checked( 1, $atts['value'], true ); ?>
		class="<?php echo esc_attr( $atts['class'] ); ?>"
		id="<?php echo esc_attr( $atts['id'] ); ?>"
		name="<?php echo esc_attr( $atts['name'] ); ?>"
		type="checkbox"
		value="1" />
	<span class="description"><?php esc_html_e( $atts['description'], 'now-hiring' ); ?></span>
</label>