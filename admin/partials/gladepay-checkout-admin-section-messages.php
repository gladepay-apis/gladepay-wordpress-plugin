<?php

/**
 * Provide a view for a section
 *
 * Enter text below to appear below the section title on the Settings page
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 *
 * @package    GladepayCheckout
 * @subpackage GladepayCheckout/admin/partials
 * @author     Gladepay <support@gladepay.com>
 */

?><p></p>