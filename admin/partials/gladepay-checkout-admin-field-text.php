<?php

/**
 * Provides the markup for any text field
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 *
 * @package    GladepayCheckout
 * @subpackage GladepayCheckout/admin/partials
 * @author     Gladepay <support@gladepay.com>
 */

if ( ! empty( $atts['label'] ) ) {

	?><label for="<?php echo esc_attr( $atts['id'] ); ?>"><?php esc_html_e( $atts['label'], 'gladepay-checkout' ); ?>: </label><?php

}

?><input
	class="<?php echo esc_attr( $atts['class'] ); ?>"
	id="<?php echo esc_attr( $atts['id'] ); ?>"
	name="<?php echo esc_attr( $atts['name'] ); ?>"
	placeholder="<?php echo esc_attr( $atts['placeholder'] ); ?>"
	type="<?php echo esc_attr( $atts['type'] ); ?>"
	value="<?php echo esc_attr( $atts['value'] ); ?>" /><?php

if ( ! empty( $atts['description'] ) ) {

	?><br><span class="description"><?php esc_html_e( $atts['description'], 'gladepay-checkout' ); ?></span><?php

}