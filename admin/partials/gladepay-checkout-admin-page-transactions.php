<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 *
 * @package    GladepayCheckout
 * @subpackage GladepayCheckout/admin/partials
 * @author     Gladepay <support@gladepay.com>
 */

    global $wpdb;

    $appTable = $wpdb->prefix . "my_table";
    $query = $wpdb->prepare("SELECT * FROM $appTable WHERE %d >= '0'", RID);
    $applications = $wpdb->get_results($query);

?>
<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>
    </thead>
    <?php
        foreach ( $applications as $application ) {
            echo'<tr>' 
                .'<td>'.$application->id.'</td>'
                .' <td>'.$application->name.'</td>'
                .' </tr>';
        }
    ?>

</table>