<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 *
 * @package    GladepayCheckout
 * @subpackage GladepayCheckout/admin/partials
 * @author     Gladepay <support@gladepay.com>
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    GladePayCheckout
 * @subpackage GladePayCheckout/admin
 * @author     Gladepay <suuport@gladepay.com>
 */
class GladePayCheckout_Admin {

	/** Define plugin constants */
    const MIN_PHP_VERSION  = '5.2.4';
	const MIN_WP_VERSION   = '3.0';
	
	/**
	 * The plugin options.
	 *
	 * @since 		1.0.0
	 * @access 		private
	 * @var 		string 			$options    The plugin options.
	 */
	private $options;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name      The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->set_options();

	}

	/**
     * Adds notices for the admin to display.
     * Saves them in a temporary plugin option.
     * This method is called on plugin activation, so its needs to be static.
     */
    public static function add_admin_notices() {

    	$notices 	= get_option( 'gladepay_checkout_deferred_admin_notices', array() );
  		//$notices[] 	= array( 'class' => 'updated', 'notice' => esc_html__( 'Gladepay Checkout: Custom Activation Message', 'now-hiring' ) );
  		//$notices[] 	= array( 'class' => 'error', 'notice' => esc_html__( 'Gladepay Checkout: Problem Activation Message', 'now-hiring' ) );

  		apply_filters( 'gladepay_checkout_admin_notices', $notices );
  		update_option( 'gladepay_checkout_deferred_admin_notices', $notices );

	} // add_admin_notices
	
	/**
     * Manages any updates or upgrades needed before displaying notices.
     * Checks plugin version against version required for displaying
     * notices.
     */
	public function admin_notices_init() {

		$current_version = '4.0.0';

		if ( $this->version !== $current_version ) {

			// Do whatever upgrades needed here.

			update_option('my_plugin_version', $current_version);

			$this->display_admin_notices();

		}

	} // admin_notices_init()


	/**
     * Displays a warning when installed on an old PHP version.
     */
    public function php_version_error()
    {
        echo '<div class="error notice"><p><strong>';
        printf(
            'Error: %3$s requires PHP version %1$s or greater to work efficiently.<br/>'.
            'Your installed PHP version: %2$s',
            self::MIN_PHP_VERSION,
            PHP_VERSION,
            $this->plugin_name
        );
        echo '</strong></p></div>';
    } //php_version_error

    /**
     * Displays a warning when installed in an old Wordpress version.
     */
    public function wp_version_error()
    {
        echo '<div class="error notice"><p><strong>';
        printf(
            'Error: %2$s requires WordPress version %1$s or greater to work efficiently.',
            self::MIN_WP_VERSION,
            $this->plugin_name
        );
        echo '</strong></p></div>';
	} //wp_version_error
	

	/**
     * Displays a warning when test mode is chosen on gladepay settings.
     */
    public function gp_test_warning()
    {
        echo '<div class="notice notice-warning is-dismissible"><p><strong>';
        printf(
            'Warning: Gladepay test mode is still enabled, Go to settings to change it when you want to start accepting live payment on your site.'
        );
        echo '</strong></p></div>';
    } //gp_test_warning



	/**
	 * Adds a settings page link to a menu
	 *
	 * @link 		https://codex.wordpress.org/Administration_Menus
	 * @since 		1.0.0
	 * @return 		void
	 */

	public function add_menu() {

		// Top-level page
		// add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

		// Submenu Page
		// add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);

		add_menu_page( 
			'Gladepay Settings', 
			'Gladepay Settings', 
			'manage_options', 
			$this->plugin_name . '-settings', 
			array( $this, 'page_options'),
			plugin_dir_url( __FILE__ ) . '/img/gladepay.png', 
			4
		);

		/* add_submenu_page(
			$this->plugin_name . '-settings',
			apply_filters( $this->plugin_name . '-settings-page-title', esc_html__( 'Gladepay Checkout Settings', 'gladepay-checkout' ) ),
			apply_filters( $this->plugin_name . '-settings-menu-title', esc_html__( 'Settings', 'gladepay-checkout' ) ),
			'manage_options',
			$this->plugin_name . '-options',
			array( $this, 'page_options' )
		); */


		add_submenu_page(
			$this->plugin_name . '-settings',
			apply_filters( $this->plugin_name . '-settings-page-title', esc_html__( 'Gladepay Checkout Help', 'gladepay-checkout' ) ),
			apply_filters( $this->plugin_name . '-settings-menu-title', esc_html__( 'Help', 'gladepay-checkout' ) ),
			'manage_options',
			$this->plugin_name . '-help',
			array( $this, 'page_help' )
		);
		
	} // add_menu()

	/**
	 * Creates the options page
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */
	public function page_options() {

		include( plugin_dir_path( __FILE__ ) . 'partials/gladepay-checkout-admin-page-settings.php' );

	} // page_options()

	
	/**
	 * Creates the help page
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */
	public function page_help() {

		include( plugin_dir_path( __FILE__ ) . 'partials/gladepay-checkout-admin-page-help.php' );

	}


	/**
	 * Registers settings sections with WordPress
	 */
	public function register_sections() {

		// add_settings_section( $id, $title, $callback, $menu_slug );

		add_settings_section(
			$this->plugin_name . '-general',
			apply_filters( $this->plugin_name . 'section-title-messages', esc_html__( 'General Settings', 'gladepay-chekout' ) ),
			array( $this, 'section_messages' ),
			$this->plugin_name
		);

		add_settings_section(
			$this->plugin_name . '-credentials',
			apply_filters( $this->plugin_name . 'section-title-messages', esc_html__( 'Credential Settings', 'gladepay-chekout' ) ),
			array( $this, 'section_messages' ),
			$this->plugin_name
		);

		add_settings_section(
			$this->plugin_name . '-branding',
			apply_filters( $this->plugin_name . 'section-title-messages', esc_html__( 'Checkout Branding', 'gladepay-chekout' ) ),
			array( $this, 'section_messages' ),
			$this->plugin_name
		);

		add_settings_section(
			$this->plugin_name . '-redirect',
			apply_filters( $this->plugin_name . 'section-title-messages', esc_html__( 'Redirect Pages', 'gladepay-chekout' ) ),
			array( $this, 'section_messages' ),
			$this->plugin_name
		);

	} // register_sections()

	/**
	 * Registers plugin settings
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */
	public function register_settings() {

		// register_setting( $option_group, $option_name, $sanitize_callback );

		register_setting(
			$this->plugin_name . '-options',
			$this->plugin_name . '-options',
			array( $this, 'validate_options' )
		);

	} // register_settings()
	

	private function sanitizer( $type, $data ) {

		if ( empty( $type ) ) { return; }
		if ( empty( $data ) ) { return; }

		$return 	= '';
		$sanitizer 	= new GladePayCheckout_Sanitize();

		$sanitizer->set_data( $data );
		$sanitizer->set_type( $type );

		$return = $sanitizer->clean();

		unset( $sanitizer );

		return $return;

	} // sanitizer()


	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in GladePayCheckout_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The GladePayCheckout_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gl-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in GladePayCheckout_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The GladePayCheckout_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gl-admin.js', array( 'jquery' ), $this->version, false );

	}

	
	/**
	 * Registers settings fields with WordPress
	 */
	public function register_fields() {

		// add_settings_field( $id, $title, $callback, $menu_slug, $section, $args );

		add_settings_field(
			'gateway-settings',
			apply_filters( $this->plugin_name . 'label-test-live', esc_html__( 'Gateway Settings', 'gladepay-checkout' ) ),
			array( $this, 'field_select' ),
			$this->plugin_name,
			$this->plugin_name . '-general',
			array(
				'description' 	=> 'Test mode enables you to test payments before going live. Once you go LIVE on your Gladepay account, please change this settings to live',
				'id' 			=> 'gateway-settings',
				'selections' 	=> 	array(
										'Test' => 'Test',
										'Live' => 'Live',
									)
			)
		);
		

		add_settings_field(
			'merchant-id',
			apply_filters( $this->plugin_name . 'label-merchant-id', esc_html__( 'Merchant ID', 'gladepay-checkout' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-credentials',
			array(
				'description' 	=> 'Login into your dashboard with your login parameters & Click on API Credentials under Settings.',
				'id' 			=> 'merchant-id',
				'placeholder'	=> 'Paste your Merchant ID here'
			)
		);

		add_settings_field(
			'key',
			apply_filters( $this->plugin_name . 'label-key', esc_html__( 'Merchant Key', 'gladepay-checkout' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-credentials',
			array(
				'description' 	=> 'Login into your dashboard with your login parameters & Click on API Credentials under Settings.',
				'id' 			=> 'key',
				'placeholder'	=> 'Paste your Merchant Key here'
			)
		);


		/* Branding Section */
		add_settings_field(
			'checkout-logo',
			apply_filters( $this->plugin_name . 'label-checkout-logo', esc_html__( 'Your Logo Url', 'gladepay-checkout' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-branding',
			array(
				'description' 	=> 'Logo that will be displayed on your checkout.',
				'id' 			=> 'checkout-logo',
				'type' 			=> 'url',
				'placeholder'	=> 'e.g. https://www.yoursiteaddress/favicon.png'
			)
		);

		add_settings_field(
			'checkout-title',
			apply_filters( $this->plugin_name . 'label-checkout-title', esc_html__( 'Title', 'gladepay-checkout' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-branding',
			array(
				'description' 	=> 'Title that will be displayed on your checkout page.',
				'id' 			=> 'checkout-title',
				'placeholder'	=> 'e.g. My Website name'
			)
		);

		add_settings_field(
			'checkout-desc',
			apply_filters( $this->plugin_name . 'label-checkout-desc', esc_html__( 'Description', 'gladepay-checkout' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-branding',
			array(
				'description' 	=> 'Description that will displayed on the checkout page.',
				'id' 			=> 'checkout-desc',
				'placeholder'	=> 'This will be displayed on the checkout form'
			)
		);

		
		
		/* Redirect Section */
		add_settings_field(
			'success-page',
			apply_filters( $this->plugin_name . 'label-success-page', esc_html__( 'Success Redirect Page', 'gladepay-checkout' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-redirect',
			array(
				'description' 	=> 'Redirect to page link after a successful payment (Leave blank if you want our default popup message)',
				'id' 			=> 'success-page',
				'type' 			=> 'url',
				'placeholder'	=> 'e.g. https://www.yoursiteaddress/success-payment-page'
			)
		);


		add_settings_field(
			'failed-page',
			apply_filters( $this->plugin_name . 'label-failed-page', esc_html__( 'Failed Redirect Page', 'gladepay-checkout' ) ),
			array( $this, 'field_text' ),
			$this->plugin_name,
			$this->plugin_name . '-redirect',
			array(
				'description' 	=> 'Redirect to page link after failed payment (Leave blank if you want our default popup message)',
				'id' 			=> 'failed-page',
				'type' 			=> 'url',
				'placeholder'	=> 'e.g. https://www.yoursiteaddress/failed-payment-page'
			)
		);

	} // register_fields()


	/**
	 * Creates a select field
	 *
	 * Note: label is blank since its created in the Settings API
	 *
	 * @param 	array 		$args 			The arguments for the field
	 * @return 	string 						The HTML field
	 */
	public function field_select( $args ) {
		$defaults['aria'] 			= '';
		$defaults['blank'] 			= '';
		$defaults['class'] 			= 'regular-text';
		$defaults['context'] 		= '';
		$defaults['description'] 	= '';
		$defaults['label'] 			= '';
		$defaults['name'] 			= $this->plugin_name . '-options[' . $args['id'] . ']';
		$defaults['selections'] 	= array();
		$defaults['value'] 			= '';

		apply_filters( $this->plugin_name . '-field-select-options-defaults', $defaults );

		$atts = wp_parse_args( $args, $defaults );

		if ( ! empty( $this->options[$atts['id']] ) ) {

			$atts['value'] = $this->options[$atts['id']];

		}

		if ( empty( $atts['aria'] ) && ! empty( $atts['description'] ) ) {

			$atts['aria'] = $atts['description'];

		} elseif ( empty( $atts['aria'] ) && ! empty( $atts['label'] ) ) {

			$atts['aria'] = $atts['label'];

		}

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-admin-field-select.php' );

	} // field_select()


	/**
	 * Creates a text field
	 *
	 * @param 	array 		$args 			The arguments for the field
	 * @return 	string 						The HTML field
	 */
	public function field_text( $args ) {
		$defaults['class'] 			= 'regular-text'; //or use 'text widefat' for full width 
		$defaults['description'] 	= '';
		$defaults['label'] 			= '';
		$defaults['name'] 			= $this->plugin_name . '-options[' . $args['id'] . ']';
		$defaults['placeholder'] 	= '';
		$defaults['type'] 			= 'text';
		$defaults['value'] 			= '';

		apply_filters( $this->plugin_name . '-field-text-options-defaults', $defaults );

		$atts = wp_parse_args( $args, $defaults );

		if ( ! empty( $this->options[$atts['id']] ) ) {

			$atts['value'] = $this->options[$atts['id']];

		}

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-admin-field-text.php' );

	} // field_text()

	/**
	 * Returns an array of options names, fields types, and default values
	 *
	 * @return 		array 			An array of options
	 */
	public static function get_options_list() {

		$options = array();

		$options[] = array( 'gateway-settings', 'select', 'test' );
		$options[] = array( 'merchant-id', 'text', '');
		$options[] = array( 'key', 'text', '');
		$options[] = array( 'checkout-logo', 'url', '');
		$options[] = array( 'checkout-title', 'text', '');
		$options[] = array( 'checkout-desc', 'text', '');
		$options[] = array( 'success-page', 'url', '');
		$options[] = array( 'failed-page', 'url', '');

		return $options;

	} // get_options_list()

	/**
	 * Creates a settings section
	 *
	 * @since 		1.0.0
	 * @param 		array 		$params 		Array of parameters for the section
	 * @return 		mixed 						The settings section
	 */
	public function section_messages( $params ) {
		// var_dump($params);
		include( plugin_dir_path( __FILE__ ) . 'partials/gladepay-checkout-admin-section-messages.php' );
		
	} // section_messages()


	
	/**
	 * Sets the class variable $options
	 */
	private function set_options() {

		$this->options = get_option( $this->plugin_name . '-options' );

	} // set_options()

	/**
	 * Validates saved options
	 *
	 * @since 		1.0.0
	 * @param 		array 		$input 			array of submitted plugin options
	 * @return 		array 						array of validated plugin options
	 */
	public function validate_options( $input ) {

		// wp_die( print_r( $input ) );

		$valid 		= array();
		$options 	= $this->get_options_list();

		foreach ( $options as $option ) {

			$name = $option[0];
			$type = $option[1];

			$valid[$option[0]] = $this->sanitizer( $type, $input[$name] );
		}

		// wp_die( print_r( $options ) );
		return $valid;

	} // validate_options()
}
