=== Gladepay Checkout ===
Contributors: solamichealolawale
Donate link: https://www.gladepay.com/demo
Tags: payment gateway, payments, shortcode, gladepay, naira payments, naira, dollar, checkout.
Requires at least: 3.0.1
Tested up to: 4.8
Stable tag: 1.0
Requires PHP: 5.2.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily add Gladepay's payment form to any page or post using simple shortcode. A few other customization options are available as well.

== Description ==

Easily add Gladepay's payment form to any page or post using simple shortcode.

Can be used on pages, custom pages, post and on custom posts

A few other customization options are available as well to suite your own taste or need; this plugin just makes it easy for anyone to emebed Gladepay payment gateway form into pages or other areas of the site without having to modify theme files.

Plugin is depending upon your theme's styling; version 1.0 of this plugin does not contain native styles.

This is a minimal plugin, function over form. If you would like to extend it, or would like us to extend it in later versions, please post feature suggestions in the plugin's support forum or contact us.

Give us feedback and contribute to this plugin on its GitHub page


== Installation ==

You can install from within WordPress using the Plugin/Add New feature, or if you wish to manually install:

1. Download the plugin.
2. Upload `gladepay-checkout` folder to the `/wp-content/plugins/` directory
3. Activate the plugin from the plugin page in your WordPress Dashboard
4. Start embedding Gladepay form in whatever pages you like using shortcode.

== Plugin Usage ==

'The simplest version of the shortcode is:
 `[Gladepay]` - Displays Gladepay payment form on the website (while all required info will be displayed).

There are currently seven attributes that can be added to the shortcode to help receiving your payments:
* firstname
* lastname
* email
* phone
* amount
* currency
* country

`[gladepay firstname="John" lastname="Doe" email="doe@example.com" phone="08098765432" amount="20000" currency="NGN" country="NG"]` - An example with all the default attributes used.

= firstname =
This will be the first name of your customer, but you can skip this attribute if you don't want a fixed last name

= lastname =
This will be the last name of your customer, but you can skip this attribute if you don't want a fixed last name

= email *(required) = 
This will be the email address of your customer, we will send a payment receipt to this email. So, it is required of user to fill this part on the form.

= phone =
This will be the phone number of your customer, but you can skip this attribute if you don't want a phone number

= amount =
This will be the amount you want your customer to pay, but you can skip this attribute if you don't want your customer to pay a fixed amount.
e.g. `[Gladepay amount="2000"]` - (will prompt your customer pay an amount value of of 2000 on the form).


= currency =
Determines the currency you want your customers to pay with.
e.g.  `[Gladepay currency="NGN"`  - (customers pay in Nigeran Naira).

= country = 
Determines which country you want to receive payments with.
e.g. `country="NG"` - (Nigeria).


== Frequently Asked Questions ==

= What is the point of this plugin? =

Some of our merchants wanted to embed Gladepay form for posts or a specific page without fiddling with the templates.

You can also use this plugin for donation on your website or you want to collect payment from your clients/customers.

= Can I use it for WooCommerce? =

No. Gladepay have another WordPress Plugin that can handle that for you.


= Can I use it on any page? =

Absolutely.

= How about with post pages? =

You bet.

= Can I get a demonstration? =

Of course you can, Our [Demo page](https://gladepay.com/demo "Seeing is believing").


== Screenshots ==

1. Visit our [Demo page](https://gladepay.com/demo "Seeing is believing").

== Changelog ==

= 1.0 =
* Still fresh!

== Upgrade Notice ==

= 1.0 =
Updated the Demo Merchant Key and Demo Merchant ID.
