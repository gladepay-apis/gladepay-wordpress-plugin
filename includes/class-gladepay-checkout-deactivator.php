<?php
/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 * 
 * @package    GladePayCheckout
 * @subpackage GladePayCheckout/includes
 * @author     Gladepay <support@gladepay.com>
 */
class GladePayCheckout_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
