<?php
/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 * 
 * @package    GladePayCheckout
 * @subpackage GladePayCheckout/includes
 * @author     Gladepay <support@gladepay.com>
 */
class GladePayCheckout_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'plugin-name',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
