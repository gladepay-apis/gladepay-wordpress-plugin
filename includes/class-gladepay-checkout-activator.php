<?php
/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 * 
 * @package    GladePayCheckout
 * @subpackage GladePayCheckout/includes
 * @author     Gladepay <support@gladepay.com>
 */

class GladePayCheckout_Activator {

	/**
	 * Flushes rewrite rules
	 * Declare plugin settings
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-gladepay-checkout-admin.php';

		flush_rewrite_rules();

		$opts 		= array();
		$options 	= GladePayCheckout_Admin::get_options_list();

		foreach ( $options as $option ) {

			$opts[ $option[0] ] = $option[2];

		}

		update_option( 'gladepay-checkout-options', $opts );

		GladePayCheckout_Admin::add_admin_notices();

	}

}
