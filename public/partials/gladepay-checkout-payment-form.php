<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://gladepay.com
 * @since      1.0.0
 *
 * @package    GladepayCheckout
 * @subpackage GladepayCheckout/public/partials
 */

    // Save each attribute's value to its own variable.
    // This creates a variable $align with a value of 'left'.
    extract( 
        shortcode_atts( array(
            'firstname' => null,
            'lastname' => null,
            'email' => null,
            'phone' => null,
            'amount' => null, 
            'currency' => null,
            'country' => null 

        ), $atts)
    );
    
    // from the admin dashboard
    $settings = $this->get_admin_field('gateway-settings');
    $mid = $this->get_admin_field('merchant-id');
    $key = $this->get_admin_field('key');
    $logo = $this->get_admin_field('checkout-logo');
    $title = $this->get_admin_field('checkout-title');
    $desc = $this->get_admin_field('checkout-desc');
    $success = $this->get_admin_field('success-page');
    $failed = $this->get_admin_field('failed-page');

    if (!empty($firstname)) {
        $new_firstname = '<input type="hidden" name="firstname" value="'.$firstname.'" id="GP-checkout-firstname"/>';
    } else {
        $new_firstname = '<label for="name" id="firstnamelabel">First Name:</label>'
                        .'<input type="text" class="GP-checkout-textbox" name="firstname" required id="GP-checkout-firstname"/>';
    }

    if (!empty($lastname)) {
        $new_lastname = '<input type="hidden" name="lastname" value="'.$lastname.'" id="GP-checkout-lastname"/>';
    } else {
        $new_lastname = '<label for="name" id="lastnamelabel">Last Name:</label>'
                        .'<input type="text" class="GP-checkout-textbox" name="lastname" required id="GP-checkout-lastname"/>';
    }

    if (!empty($email)) {
        $new_email = '<input type="hidden" name="email" value="'.$email.'" id="GP-checkout-email"/>';
    } else {
        $new_email = '<label for="email" id="emailabel">E-mail:</label>'
                    .'<input type="email" name="email" class="GP-checkout-textbox" required id="GP-checkout-email"/>';
    }
    
    if (!empty($amount)) {
        $new_amount = '<input type="hidden" name="amount" value="'.$amount.'" id="GP-checkout-amount"/>';
    } else {
        $new_amount = '<label for="amount" id="amountabel">Amount:</label>'
                        .'<input type="number" name="amount" class="GP-checkout-textbox" required id="GP-checkout-amount"/>';
    }

    if (!empty($phone)) {
        $new_phone = '<input type="hidden" name="phone" value="'.$phone.'" id="GP-checkout-phone"/>';
    } else {
        $new_phone = '<label for="phone" id="phonelabel">Phone Number:</label>'
                    .'<input type="number" name="phone" class="GP-checkout-textbox" required id="GP-checkout-phone"/>';
    }

    if (!empty($country)) {
        $new_country = '<input type="hidden" name="country" value="'.$country.'" id="GP-checkout-country"/>';
    }else{
        $new_country = '<label for="country" id="countrylabel">Choose Country:</label>'
                        .'<select id="country" name="GP-checkout-country" required class="GP-checkout-select">'
                            .'<option value="NG">Nigeria</option>'
                            // .'<option value="KYN">Kenya</option>'
                            // .'<option value="SA">South Africa</option>'
                        .'</select>';
    }

    if (!empty($currency)) {
        $new_currency = '<input type="hidden" name="currency" value="'.$currency.'" id="GP-checkout-curreny"/>';
    }else{
        $new_currency = '<label for="currency" id="currencylabel">Choose Currency:</label>'
                        .'<select id="GP-checkout-currency" name="currency" required class="GP-checkout-select">'
                            .'<option value="NGN">NGN</option>'
                            .'<option value="USD">USD</option>'
                            // .'<option value="KES">KES</option>'
                            // .'<option value="GHS">GHS</option>'
                            // .'<option value="CFA">CFA Franc</option>'
                            // .'<option value="GBP">GBP</option>'
                            // .'<option value="EUR">EUR</option>'
                        .'</select>';
    }

    if(!empty($mid)){
        $new_mid = '<input type="hidden" name="mid" required id="GP-checkout-mid" value="'.$mid.'">';
    }else{
        $new_mid = '<div class="GP-checkout-notification danger">'
                        .'Please provide your Merchant ID in the admin dashboard'
                    .'</div>';
    }

    if(!empty($key)){
        $new_key = '<input type="hidden" name="key" required id="GP-checkout-key" value="'.$key.'">';
    }else{
        $new_key = '<div class="GP-checkout-notification danger" style="display:none">'
                        .'Please provide your Merchant key in the admin dashboard.'
                    .'</div>';
    }

    if(!empty($logo)){
        $new_logo = '<input type="hidden" name="logo" required id="GP-checkout-logo" value="'.$logo.'">';
    }else{
        $new_logo = '';
    }
    if(!empty($title)){
        $new_title = '<input type="hidden" name="title" required id="GP-checkout-title" value="'.$title.'">';
    }else{
        $new_title = '';
    }

    if(!empty($desc)){
        $new_desc = '<input type="hidden" name="desc" required id="GP-checkout-desc" value="'.$desc.'">';
    }else{
        $new_desc = '';
    }
    
    if(!empty($success)){
        $new_success = '<input type="hidden" name="success-page" required id="GP-checkout-success" value="'.$success.'">';
     }else{
        $new_success = '';
    }

    if(!empty($failed)){
        $new_failed = '<input type="hidden" name="failed-page" required id="GP-checkout-failed" value="'.$failed.'">';
     }else{
        $new_failed = '';
    }
   

    if( !('test' == $settings) ){
        $script = '<script type="text/javascript" src="https://api.gladepay.com/checkout.js"></script>';
    }else{
        $script = '<script type="text/javascript" src="https://demo.api.gladepay.com/checkout.js"></script>';
    }
?>

<?php echo $script; ?>
<form id="GP-checkout-form" name="form">
    <h3 class="entry-title"> Gladepay Payment Form </h3>
    <div class="GP-checkout-notification danger" id="key_alert" style="display:none"></div>
    <?php  
        echo $new_firstname;
        echo $new_lastname;
        echo $new_email;
        echo $new_amount;
        echo $new_phone;
        echo $new_currency;
        echo $new_country;
        echo $new_mid;
        echo $new_key;
        echo $new_logo;
        echo $new_title;
        echo $new_desc;
        echo $new_success;
        echo $new_failed;
    ?>
    <input type="submit" name="GP-checkout-btn" id="GP-checkout-btn" class="GP-checkout-btn" value="Pay">
</form>
