

(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).ready(function(){
			
		$('form#GP-checkout-form').on('submit', function (e) {
			e.preventDefault();
			var mid = $('#GP-checkout-mid').val(),
				key = $('#GP-checkout-key').val(),
				logo = $('#GP-checkout-logo').val(),
				title = $('#GP-checkout-title').val(),
				desc = $('#GP-checkout-desc').val(),
				email = $('#GP-checkout-email').val(),
				firstname = $('#GP-checkout-firstname').val(),
				lastname = $('#GP-checkout-lastname').val(),
				amount = $('#GP-checkout-amount').val(),
				phone = $('#GP-checkout-phone').val(),
				country = $('#GP-checkout-country').val(),
				success_page = $('#GP-checkout-success').val(),
				failed_page = $('#GP-checkout-failed').val(),
				currency = $('#GP-checkout-currency').val();
			
			initPayment({
				MID: mid,
				email: email,
				firstname: firstname,
				lastname: lastname,
				description: desc,
				logo: logo,
				title: title,
				amount: amount,
				theme: "yellowsun",
				phone: phone,
				country: country,
				currency: currency,
				onclose: function () {
					
				},
				callback: function (response) {
					// console.log(response);
					if (
						response.status == 200 ||
						response.txnStatus == "successful"
					) {
						if ( success_page ){
							window.location.href = success_page;
						}else{
							alert('Payment Successful.\n Thank you for paying!');
							location.reload(true);
						}
					} else {
						if ( failed_page) {
							window.location.href = failed_page;
						} else {
							alert('Payment Failed.\n Please try again later.');
							location.reload();
						}
					}
				}
			});
		});



	});

})( jQuery );
